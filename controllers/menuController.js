var dbconfig = require('../util/dbconfig.js');
getMenu=(req,res)=>{
    var sql="select * from menu";
    var sqlArr=[];
    var callBack=(err,data)=>{
      if(err){
        console.log("连接出错")
      }else{
        res.send({
          'list':data
        });
      }
    };
    dbconfig.sqlConnect(sql,sqlArr,callBack)
}
getCateMenu=(req,res)=>{
    var {id} = req.query
    var sql="select * from menu where qx=?";
    var sqlArr=[id];
    var callBack=(err,data)=>{
      if(err){
        console.log("连接出错")
      }else{
        res.send({
          'list':data
        });
      }
    };
    dbconfig.sqlConnect(sql,sqlArr,callBack)
}

module.exports={
    getMenu,getCateMenu
}