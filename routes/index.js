var express = require('express');
var router = express.Router();
var menu = require('../controllers/menuController');
/* 注册 */
router.get('/', menu.getMenu);
router.get('/getCateMenu', menu.getCateMenu);

module.exports = router;
