const mysql = require("mysql")

module.exports={
    config:{
        host:'localhost',
        port:'3306',
        user:'root',
        password:'root',
        database:'nodetest',
    },
    //连接池
    sqlConnect:function(sql,sqlArr,callBack){
        var pool = mysql.createPool(this.config)
        pool.getConnection((err,conn)=>{
            console.log('laole');
            if(err){
                console.log("连接失败")
                return
            }
            //事件驱动回调
            conn.query(sql,sqlArr,callBack);
            //释放连接
            conn.release()
        })
    },
    //promise回调
    sySqlConnect:function(sysql,sqlArr){
        return new Promise((resolve,reject)=>{
            var pool = mysql.createPool(this.config)
            pool.getConnection((err,conn)=>{
                console.log('laole');
                if(err){
                    reject(err)
                }
                //事件驱动回调
                conn.query(sysql,sqlArr,(err,data)=>{
                    if(err){
                        reject(err)
                    }else{
                        resolve(data)
                    }
                });
                //释放连接
                conn.release()
            })
        }).catch((err)=>{
            console.log(err)
        })
    }
}